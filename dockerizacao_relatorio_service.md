# Relatorio-Service: Gerando a imagem e rodando container em um server local
___
O intuito da seguinte documentação é mostrar o procedimento geral, bem como as eventuais peculiaridades, de como gerar uma imagem docker do serviço gerador de relatório através do spring-boot e, de posse dessa imagem, como executá-la em um container docker rodando em um servidor local.

## Requisitos Necessários
___
Para a execução desse tutorial, assume-se como satisfeitas as seguintes premissas:

**1- Ter acesso a um ambiente Linux**
No caso deste tutorial, será utilizado o *Windows 10 + WSL versão 2*.  Com isso, teremos acesso a um ambiente Linux através do próprio Windows. A distro escolhida para se trabalhar foi a Ubuntu 20.04.4.
**Obs:** *Apesar de termos escolhido o WSL, nada impede a utilização de outros meios como Hyper-V, Oracle Virtual Box ou até mesmo uma máquina física rodando Linux SO.*

**2 - Ter o docker instalado em seu ambiente Linux**
Este requisito pode ser satisfeito por meio desse script aqui -> [instalacao_docker](https://gitlab.com/manoelallam/versao-java-docker/-/blob/main/instalcao_docker.bash)

**3- Ter o JDK17 instalado em ambiente Linux**

**4- Ter o projeto do relatorio-service em sua máquina local e com o branch master atualizado**

## Executando relatorio-service em ambiente Linux
___
Para que a imagem seja gerada de forma saudável, é imprescindível que o seu projeto do relatorio-service esteja rodando normalmente.

Como nessa documentação faremos tudo no lado Linux, então essa aplicação precisa estar rodando corretamente nesse ambiente.

Contudo, ao tentar executar um simples `mvn clean` no projeto relatorio-service com o maven 3.6, obtemos o seguinte erro:

    [ERROR] Error executing Maven.
    [ERROR] java.lang.IllegalStateException: Unable to load cache item
    [ERROR] java.lang.IllegalStateException: Unable to load cache item
    [ERROR] Caused by: Unable to load cache item[ERROR] Caused by: Could not initialize class com.google.inject.internal.cglib.core.$ReflectUtils`

Isso ocorre por conta de uma incompatibilidade entre a versão utilizada do maven e a versão 17 do JDK. Esse é um bug que, via de regra, ocorre em ambientes Linux e já é bem conhecido da comunidade (https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=980467).

Para solucioná-lo, precisamos atualizar nossa versão do Maven, visto que, fazer um downgrade da versão do JDK está fora de cogitação.

### Instalando Maven 3.8.5
Para instalação do Maven 3.8.5, basta seguir a seguinte sequência de passos:
1. Fazer o download do Maven no site oficial ([link](https://dlcdn.apache.org/maven/maven-3/3.8.5/binaries/apache-maven-3.8.5-bin.tar.gz))`
2. Em seu ambiente Linux, realizar a extração do arquivo em uma pasta de sua escolha, por meio do comando:
   ```tar xzvf apache-maven-3.8.5-bin.tar.gz```
3. Fazer as variáveis de ambiente `$M2_HOME`,`$M2` e `$PATH` apontarem para a pasta onde o Maven recém instalado e extraído se encontra, bem como atribuir algumas configurações de execução da JVM usando Maven. Isso pode ser feito por meio dos comandos:
```export M2_HOME=/mnt/c/users/breno/Meus\ Documentos/apache-maven-3.8.5
export M2_HOME=/mnt/c/users/breno/Meus\ Documentos/apache-maven-3.8.5
export M2=$M2_HOME/bin
export MAVEN_OPTS="-Xms256m -Xmx512m"
export PATH=$M2:$PATH
```

Feito isso, ao executar `mvn -v` no terminal, espera-se ver o seguinte:
![enter image description here](https://i2.paste.pics/59f45ad4bf61210d122c0109f77a21cd.png?trs=a64b19d0f152077932830cf16c452f87318be97e87d4823511f5deb9c9c707df)

> Obs: *Para não perder os valores das variáveis de ambiente toda vez que se fechar o terminal, é conveniente adicionar 
> esses comandos no arquivo `etc/profile`*

### Configurando o Settings.xml
Uma vez que o Maven estiver atualizado, agora precisamos configurar o arquivo `settings.xml` da nossa nova versão do Maven para, assim, termos as permissões necessárias para conseguirmos baixar as dependências do projeto.
Para isso, vá no diretório onde o Maven foi extraído e, no arquivo `conf/settings.xml` substitua o conteúdo do arquivo pelo código a seguir, lembrando de adicionar o seu token privado que foi gerado no gitlab (ele muito provavelmente deve estar em `seu_user/.m2/settings.xml`)

    <settings>
      <servers>
        <server>
          <id>gitlab-maven</id>
          <configuration>
            <httpHeaders>
              <property>
                <name>Private-Token</name>
                <value>SEU_TOKEN_JAZ_AQUI</value>
              </property>
            </httpHeaders>
          </configuration>
        </server>
      </servers>
    </settings>


### Atualizando o pom.xml
Para podermos buildar a imagem do relatorio-service usando uma versão mais recente do java, precisamos apenas fazer o projeto apontar para a versão mais recente do  `parent`. No caso, essa versão é a 18.
Assim, no pom.xml do relatorio-service basta substituirmos a versão do pom de 11 para 18. Assim, estaremos garantindo que o projeto irá rodar com java 17.

```
<parent>  
   <groupId>br.com.softsite</groupId>  
   <artifactId>geosales-maven-parent</artifactId>  
   <version>18</version>  
</parent>
```

### Mapeando DNS do ambiente pumyra
Como o relatorio-service realiza a conexão ao banco por meio do dns pumyra e no ambiente linux esse dns ainda não está mapeado no arquivo hosts, é necessário adicionar esse mapeamento em `etc/hosts`.
Ou, se preferir, alterar na própria aplicação do relatorio-service o arquivo `jdbc-dev.properties` trocando o nome pumyra da string de conexão pelo próprio endereço ip que o servidor se encontra.
`hikari.adm.url=jdbc:sqlserver://10.0.0.82:1433;databaseName=bd_ssm_adm`

### E, Finalmente, rodando o relatorio-service
Aqui, já podemos realizar a instalação das dependências, porém, cabem algumas observações.

Por algum motivo ainda não investigado, ao se fazer essa atualização das versões do Maven e do Java, alguns métodos de testes automatizados não passaram. Podemos adicionar um `-DskipTestes` para ignorá-los por hora.

Outra observação importante é que, como agora utilizamos o java 17, precisamos indicar o perfil utilizado, tal como fazemos ao baixar as dependencias de outros projetos Geosales. Fazemos isso com a adição do parâmetro -Pjre17.

Então o comando de limpeza da target + instalação das dependências fica assim:

`mvn clean install -Pjre17 -DskipTests`

Feita a instalação das dependências, executar a aplicação

`mvn spring-boot:run`

E, finalmente, a aplicação estará rodando em `localhost:8090`

> OBS: *a porta do `relatorio-service` pode ser configurada no arquivo `application.properties` adicionando a instrução `server.port=NUMERO_DA_PORTA`,
> caso não seja adicionada o serviço irá rodar na porta `8080` por padrão*

## Realizando Build + Containerização da Imagem
___
Com a variável `DOCKER_HOST` exportada para o endereço correto e com o docker daemon em execução ([vide tutorial](https://gitlab.com/manoelallam/versao-java-docker/-/blob/main/Usando_Docker.md)), podemos buildar a imagem do relatorio-service com o seguinte comando

`mvn spring-boot:build-image -DskipTests`

Uma vez que a imagem for gerada, executaremos a imagem dentro de um container que, por sua vez, subiremos em um servidor local na porta 8090 . Faremos isso com o seguinte comando docker:

`docker run -it -p8090:8090 relatorio-service:1.1.2 `

Assim, o container subirá em `localhost:8090` e estará pronto e preparado para receber requisições.